# cf-buildpack-process-supervisor-tests

![Build Status](https://ci.dachs.dog/api/v1/teams/main/pipelines/ps/jobs/system-test//badge)

Tests for [cf-buildpack-process-supervisor](https://bitbucket.org/cf-utilities/cf-buildpack-process-supervisor)

## To Test

### Pre-requisites

* Install [Ruby](https://www.ruby-lang.org/en/documentation/installation/) v2.1.x
* Install [Bundler](http://bundler.io/) into Ruby at the latest available version
* Install [CF CLI](https://github.com/cloudfoundry/cli/releases) at version <= 6.21, due to some sort of BlueMix bug that means the wrong logging endpoint is being used
* Install [Fly CLI](https://concourse.ci/downloads.html) at version 2.4.0 or later

Additionally for Windows:

* Install [MinGW](http://www.mingw.org/wiki/Getting_Started)

### Running the tests

```
# First time
$ git submodule update --init --recursive
$ bundle install

# From then on
$ CF_PASSWORD=<go look it up> \
  CF_API=https://api.eu-gb.bluemix.net \
  CF_USERNAME=grp-it-dachs-test-user1@mnscorp.onmicrosoft.com \
  CF_ORG=mands \
  CF_SPACE=dachs-dev \
  APP_DOMAIN=eu-gb.mybluemix.net \
  bundle exec rspec
```

Setting the Concourse pipeline

```
# Login to Concourse
fly --target main login --team-name main --concourse-url https://ci.dachs.dog

# Set the pipeline
fly -t main set-pipeline \
-p ps \
-c ci/pipeline.yml \
--load-vars-from=ci/vars/bluemix.yml \
--var cf_password=<go look it up> \
--var private_repo_key=<go find the key>
```
