require 'fileutils'
require 'tmpdir'
require 'securerandom'
require 'rest-client'

describe 'ProcessSupervisor' do
  let(:cf_api) { ENV.fetch('CF_API') }
  let(:cf_username) { ENV.fetch('CF_USERNAME') }
  let(:cf_password) { ENV.fetch('CF_PASSWORD') }
  let(:app_domain) { ENV.fetch('APP_DOMAIN') }
  let(:org) { ENV.fetch('CF_ORG') }
  let(:space) { ENV.fetch('CF_SPACE') }
  let(:buildpacks_file_path) { File.join(app_tmp_dir, '.buildpacks') }
  let(:ps_buildpack_uri) { "file://ignored-component/tmp/staged/app/buildpack-under-test.tgz" }
  let(:multi_buildpack_uri) { "https://bitbucket.org/cf-utilities/cf-buildpack-multi" }
  let(:env_vars) { {} }

  before(:each) do
    @cf_home = Dir.mktmpdir
    ENV['CF_HOME'] = @cf_home
    FileUtils.cp_r("#{fixture_dir}/.", app_tmp_dir)

    login_result = `cf login -a #{cf_api} -u #{cf_username} -p #{cf_password} -o #{org} -s #{space} --skip-ssl-validation`
    expect($?.success?).to be_truthy, "#{login_result}"

    expect_command_to_succeed("tar czf #{app_tmp_dir}/buildpack-under-test.tgz --exclude .git/ -C cf-buildpack-process-supervisor .")

    write_buildpacks_file

    push_app

    env_vars.each do |key, value|
      expect_command_to_succeed("cf set-env #{app_name} #{key} #{value}")
    end

    start_app
  end

  after(:each) do
    `cf delete -f #{app_name}`
    FileUtils.rm_rf @cf_home
    FileUtils.rm_rf buildpacks_file_path
  end

  describe 'an oblivious app' do
    context 'when starting a standard ruby app' do
      let(:app_tmp_dir) { Dir.mktmpdir }
      let(:app_name) { "ps-test-rhw-#{SecureRandom.uuid}" }
      let(:fixture_dir) { 'spec/system/fixtures/ruby-hello-world' }
      let(:buildpack_url) { 'https://github.com/cloudfoundry/ruby-buildpack.git#master' }

      it 'starts a long-running process' do
        # expect response
        expect_hello_world

        # expect to still be running a bit later?
        sleep 10 #???
        expect_hello_world
      end
    end
  end

  describe 'ps configured apps' do
    context 'when running an app with a Procfile' do
      let(:app_tmp_dir) { Dir.mktmpdir }
      let(:app_name) { "ps-test-service-#{SecureRandom.uuid}" }
      let(:fixture_dir) { 'spec/system/fixtures/ps-proc' }
      let(:buildpack_url) { '' }

      it 'starts the app' do
        expect_hello_world
        expect_command_to_succeed_and_output("cf logs #{app_name} --recent", "STOP THIS service is running")
      end
    end

    context 'when preventing a service from running' do
      let(:app_tmp_dir) { Dir.mktmpdir }
      let(:app_name) { "ps-test-service-#{SecureRandom.uuid}" }
      let(:fixture_dir) { 'spec/system/fixtures/ps-proc' }
      let(:buildpack_url) { '' }
      let(:env_vars) {
        {
          'PS_DISABLE_SERVICE_STOP_THIS' => 'true'
        }
      }

      it 'starts the app without the service' do
        expect_hello_world
        expect_command_to_succeed_and_not_output("cf logs #{app_name} --recent", "STOP THIS service is running")
      end
    end

    context 'when running a long-running service' do
      let(:app_tmp_dir) { Dir.mktmpdir }
      let(:app_name) { "ps-test-service-#{SecureRandom.uuid}" }
      let(:fixture_dir) { 'spec/system/fixtures/ps-lrp' }
      let(:buildpack_url) { '' }

      it 'runs a service like an app' do
        expect_hello_world
      end
    end

    context 'when running a periodic service' do
      let(:app_tmp_dir) { Dir.mktmpdir }
      let(:app_name) { "ps-test-service-#{SecureRandom.uuid}" }
      let(:fixture_dir) { 'spec/system/fixtures/ps-periodic' }
      let(:buildpack_url) { '' }

      it 'runs more than once' do
        sleep 2
        output = `cf logs #{app_name} --recent`
        expect(output.scan('foo').size).to be >= 2
      end
    end
  end
end