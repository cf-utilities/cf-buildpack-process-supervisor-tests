def expect_command_to_succeed(command)
  system(command)
  expect($?.success?).to be_truthy
end

def expect_command_to_fail_and_output(command, expected)
  output = `#{command}`
  expect($?.success?).to be_falsey
  expect(output).to include(expected)
end

def expect_command_to_succeed_and_output(command, expected)
  output = `#{command}`
  expect($?.success?).to be_truthy
  expect(output).to include(expected)
end

def expect_command_to_succeed_and_not_output(command, expected)
  output = `#{command}`
  expect($?.success?).to be_truthy
  expect(output).to_not include(expected)
end

def write_buildpacks_file
  File.open(buildpacks_file_path, 'w') { |file|
    file.puts buildpack_url
    file.puts ps_buildpack_uri
  }
end

def expect_hello_world
  response = RestClient::Request.execute(method: :get, url: "https://#{app_name}.#{app_domain}", verify_ssl: OpenSSL::SSL::VERIFY_NONE)
  expect(response.code).to be(200)
  expect(response.body).to include('Hello, World!')
end

def push_app
  expect_command_to_succeed("cf push #{app_name} -p #{app_tmp_dir} -m 64M -b #{multi_buildpack_uri} --no-start")
end

def start_app
  expect_command_to_succeed("cf start #{app_name}")
  expect_command_to_succeed_and_output("cf app #{app_name}", "buildpack: #{multi_buildpack_uri}")
end